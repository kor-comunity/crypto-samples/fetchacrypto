const https = require("https");

exports.handler =  function(coinId) {
  const url = `https://api.coinpaprika.com/v1/coins/${coinId}/ohlcv/latest`;
  https.get(url, res => {
    res.setEncoding("utf8");
    let body = "";
    res.on("data", data => {
      body += data;
    });
    res.on("end", () => {
      body = body;      
      return JSON.parse(body);
    });
  });
}